FROM node:lts-alpine

WORKDIR /app
COPY . /app
COPY package.json /app/package.json

RUN yarn install

EXPOSE 8080

CMD ["yarn", "serve"]
