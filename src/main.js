import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router/'
import axios from 'axios'

Vue.config.productionTip = false
Vue.use(axios)
axios.defaults.baseURL = 'http://45.79.26.172/torre/api/'
new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
