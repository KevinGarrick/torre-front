import { DefaultLayout } from "../components/layouts"

export const publicRoute = [
  {
    path: '*',
    redirect: { name: 'main' },
  },
  {
    path: "/",
    component: DefaultLayout,
    meta: { title: "Main" },
    hidden: true,
    children: [
      {
        path: "",
        name: "main",
        component: () => import("@/components/views/main/Main.vue")
      },
    ]
  },

  // {
  //   path: "/401",
  //   name: "401",
  //   meta: { title: "No Autorizado" },
  //   component: () => import(/* webpackChunkName: "errors-401" */ "@/views/error/Unauthorized.vue")
  // },

  // {
  //   path: "/500",
  //   name: "500",
  //   meta: { title: "Server Error" },
  //   component: () => import(/* webpackChunkName: "errors-500" */ "@/views/error/Error.vue")
  // }
]

export const protectedRoute = [

]
