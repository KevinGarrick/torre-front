const openTo = {
    "full-time-employment": { "label": "Full-time Employment", "compensation": "employee" },
    "part-time-employment": { "label": "Part-time employment", "compensation": "employee" },
    "freelance-gigs": { "label": "Freelance gigs", "compensation": "freelance" },
    "internships": { "label": "Internships", "compensation": "intern" },
    "mentoring": { "label": "Mentoring others" },
    "advising": { "label": "Advising others" },
    "hiring": { "label": "Hiring talent" },
}

const periodicities = {
    "daily": "Day",
    "hourly": "Hour",
    "monthly": "Month",
    "yearly": "Year"
}

const parseOpenTo = (tag, total = 0, compensation = null) => {
    let response = {}
    //let _tag = tag
    if(!tag) return response
    const config = openTo[tag]
    
    response['label'] = config['label']
    response['total'] = totalFormat(total)
    if (!compensation || !compensation.hasOwnProperty(config['compensation'])) return response
    const compensationConfig = compensation[config['compensation']]
    response['compensation'] = `${compensationConfig['currency']}${totalFormat(compensationConfig['amount'])}/${periodicities[compensationConfig['periodicity']]}`
    return response
}
const totalFormat = (total) => {
    if (total < 1000) return total;
    if (total >= 1000 && total < 1000000)
        return buildAmmount(total, 1000, "K");
    if (total >= 1000 && total < 1000000)
        return buildAmmount(total, 1000000, "M");
}
const buildAmmount = (total, divisor, sufix) => {
    let ammount = (total / divisor).toString().split(".");
    let result = ammount[0];
    result = `${result}${sufix}`;
    return ammount.length > 1 ? `${result}+` : result;
}

export default {
    parseOpenTo,
    totalFormat
}